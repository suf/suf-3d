$fn=180;

difference()
{
    translate([-47,-47,0])
    {
        minkowski()
        {
            cube([94,94,45]);
            cylinder(d=6,h=0.001);
        }        
    }
    translate([-45,-45,3])
    {
        minkowski()
        {
            cube([90,90,42.001]);
            cylinder(d=4,h=0.001);
        }
    }
    for(j=[-5:5])
    {
        for(i=[-3:3])
        {
            translate([12 * i,j*7,-0.001])
            {
                cylinder(d=6,h=3.002,$fn=6);
            }
        }
        for(i=[-3:2])
        {
            translate([6 + 12 * i,3.5 + j * 7,-0.001])
            {
                cylinder(d=6,h=3.002,$fn=6);
            }
        }
    }
    for(i=[-3:2])
    {
        translate([6 + 12 * i,3.5 + -6 * 7,-0.001])
        {
            cylinder(d=6,h=3.002,$fn=6);
        }
    }
    for(i=[-1,1])
    {
        for(j=[-1,1])
        {
            translate([41.5 * i,41.5 * j,-0.001])
            {
                cylinder(d=5,h=3.002);
            }
        }
    }
}