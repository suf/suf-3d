$fn=360;
fan_degree=-60;
fan_z=60;
fan_x=-40;

translate([0,0,4])
{
    rotate([0,180-fan_degree,0])
    {
        translate([fan_x*-1,0,fan_z*-1])
        {
            difference()
            {
                translate([-50,-37.5,0])
                {
                    cube([100,75,5]);
                }
                cube([95,70,6],center=true);
                cube([90,65,10.002],center=true);
            }

            difference()
            {
                hull()
                {
                    translate([fan_x,0,fan_z])
                        rotate([0,fan_degree,0])
                        {
                            cylinder(d=80,h=0.001);
                        }
                    translate([0,0,5])
                        cube([100,75,0.001],center=true);
                }
                hull()
                {
                    translate([fan_x,0,fan_z])
                        rotate([0,fan_degree,0])
                        {
                            cylinder(d=75,h=0.002);
                        }
                    translate([0,0,5])
                        cube([90,65,0.003],center=true);
                }
            }

            translate([fan_x,0,fan_z])
            {
                rotate([0,fan_degree,0])
                {
                    translate([0,0,1])
                    {
                        difference()
                        {
                            minkowski()
                            {
                                cube([72,72,2],center=true);
                                cylinder(d=8,h=2);
                            }
                            translate([0,0,-1.001])
                            {
                                cylinder(d=75,h=4.002);
                                for(i=[0:3])
                                {
                                    rotate([0,0,90*i+45])
                                    {
                                        translate([sqrt(2)*36,0,0])
                                        {
                                            cylinder(d=4,h=4.002);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}