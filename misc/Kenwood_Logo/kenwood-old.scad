$fn=360;

// #cube([55,10,0.002],center=true);

module logo(logo_height)
{
    translate([-25.5,-25.5,0])
    {
        difference()
        {
            translate([0.001,0.001,0])
            cube([50.998,50.998,logo_height]);
            translate([0,0,-0.001])
            linear_extrude(height = logo_height + 0.002, convexity = 10)
               import (file = "Kenwood-old.dxf");
        }

    }
}
color([0.9,0.9,0.9])
{
    scale([1.03,1.03,1])
    {
        translate([2.5,9,2])
        {
            difference()
            {        
                logo(2);
                translate([-10,-2,-0.002]) cube([20,20,2.004]);
            }
        }

        translate([-26,2,2])
        {
            difference()
            {
                logo(2);
                translate([-30,-12,-0.002]) cube([60,10,2.004]);
            }
        }
    }
    translate([-23,0,2]) cube([49.5,1,2]);
}

color([0.25,0.25,0.25])
{
    translate([-27.5,0.5,0])
    {
        minkowski()
        {
            cube([54,6,1]);
            cylinder(d=3,h=1);
        }
    }
    translate([-27.3,6.5,0])
    {
        cylinder(d=12,h=2);
    }
}