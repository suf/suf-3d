$fn=360;
ring_dia=10;
color([0.2,0.2,0.2])
{
    difference()
    {
        hull()
        {
            translate([0,0,(ring_dia/2) +2])
            cube([10,10,1],center=true);
            rotate([90,0,0])
            {
                cylinder(d=ring_dia+4,h=5, center=true);
            }
        }
            rotate([90,0,0])
            {
                cylinder(d=ring_dia,h=10, center=true);
            }

    }
}
color("silver")
{
    translate([0,0,(ring_dia/2) + 2.5])
    {
        linear_extrude(0.5)
        {
            scale([0.025,0.025])
            {
                import(file="Rammstein_logo_2.svg",center=true);
            }
        }
    }
}
