$fn=180;
difference()
{
    union()
    {
        translate([-26,0,0])
        {
            cube([52,4,25]);
        }
        hull()
        {
            translate([-17.5,4,0])
            {
                cube([35,0.001,25]);
            }
            translate([-16,8,0])
            {
                cube([32,0.001,25]);
            }
        }
    }
    translate([-13.75,-0.001,1])
    {
        cube([27.5,1.5,24.1]);
    }
    for(i=[-1,1])
    {
        translate([i*11.75,2,13.05])
        {
            cube([4,4,24.1], center=true);
        }
        for(j=[-1,1])
        {
            translate([21*j,-0.001,12.5 + i*7.5])
            {
                rotate([-90,0,0])
                {
                    cylinder(d=3.5,h=4);
                }
                translate([0,2,0])
                {
                    rotate([-90,0,0])
                    {
                        cylinder(d=7,h=2.002);
                    }
                }
            }
        }
    }
    translate([-10.5,-0.001,22.5])
    {
        rotate([-90,0,0])
        {
            cylinder(d=3,h=8.003);
        }
    }
    translate([-10.5,4,25.5])
    {
        cube([3,8.1,6],center=true);
    }
    
    translate([10,-0.001,22.5])
    {
        rotate([-90,0,0])
        {
            cylinder(d=3,h=8.003);
        }
    }    
    translate([1,-0.001,22.5])
    {
        rotate([-90,0,0])
        {
            cylinder(d=3,h=8.003);
        }
    }

    translate([5.5,4,25.5])
    {
        cube([12,8.1,6],center=true);
    }    
    translate([5.5,4,24])
    {
        cube([9,8.1,6],center=true);
    }      
    translate([5.5,4,22])
    {
        cube([4,8.1,6],center=true);
    }      
}