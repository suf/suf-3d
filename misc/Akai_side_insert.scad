$fn=360;

difference()
{
    union()
    {
        cylinder(d=14,h=9);
        translate([0,0,7.4])
            cylinder(d=17.5,h=1.6);
    }
    translate([0,0,3.5])
        cylinder(d=10.5,h=5.501);
    translate([0,0,-0.001])
        cylinder(d=4.5,h=3.502);
}
