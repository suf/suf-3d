$fn=360;
intersection()
{
    difference()
    {
        union()
        {
            cylinder(d=6,h=4.3);
            translate([0,0,4.3])
            {
                cylinder(d=8, h=1.2);
            }
            translate([0,0,5.5])
            {
                cylinder(d=9.4, h=1);
            }
            translate([0,0,6.5])
            {
                cylinder(d=10.6, h=1);
            }
            translate([0,0,7.5])
            {
                cylinder(d=7.5, h=10);
            }
            for(i=[0:49])
            {
                rotate([0,0,360/50*i])
                {
                    translate([3.75,0,13.5])
                    {
                        rotate([0,0,45])
                        {
                            cube([0.5,0.5,8],center=true);
                        }
                    }
                }
            }
        }
        difference()
        {
            translate([0,0,-0.001])
            {
                cylinder(d=4, h=10);
            }
            translate([-2.8,-2,-0.002])
            {
                cube([2,4,10]);
            }
        }
        cube([0.8,9.402,11],center=true);
    }
    cylinder(d1=42.6,d2=0.001,h=21.3);
}