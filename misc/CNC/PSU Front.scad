
$fn=180;

difference()
{
    cube([149,86,3]);
    translate([14,5.1,0])
    {
        for(j=[1:21])
        {
            for(i=[0:(10-j%2)])
            {
                translate([12 * i + 6 * (j%2), j * 3.5, -0.001])
                {
                    cylinder(d=6, h=3.002, $fn=6);
                }
            }
        }
    }
}

for(i=[0:1])
{
    difference()
    {
        hull()
        {
            translate([3 + 123*i,0,3])
            {
                cube([20,80,0.001]);
            }
            translate([3 + 123*i,0,25])
            {
                cube([20,3,0.001]);
            }
        }
        hull()
        {
            translate([6 + 117*i,3,2.999])
            {
                cube([20,80,0.001]);
            }
            translate([6 + 117*i,3,32.999])
            {
                cube([20,3,0.001]);
            }
        }
        for(i=[0:1])
        {
            translate([18.5+i*112,-0.001,20])
            {
                rotate([-90,0,0])
                {
                    cylinder(d=3.5,h=3.002);
                    cylinder(d1=6,d2=0.001,h=3);
                }
            }
        }        
    }
}

