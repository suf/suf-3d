REEL_WIDTH = 11;
REEL_TIGHTEN = 1;
$fn=360;

module NH_Logo_Akai()
{
    translate([0,16,REEL_WIDTH-REEL_TIGHTEN - 0.001])
    {
        scale([0.5,0.5,1])
        {
            translate([-34,-34,0])
            {
                difference()
                {
                    translate([0.001,0.001,0])
                    cube([67.999,67.999,3]);
                    translate([0,0,-0.001])
                    linear_extrude(height = 3.002, convexity = 10)
                       import (file = "Akai.dxf");
                }
            }
        }
    }
}

NH_Logo_Akai();
