DIN_HUB_DIA = 8.75;
DIN_SPOKE_DIST = 7;
DIN_SPOKE_DIA = 2;
// NAB_HUB_DIA = 75.3;
NAB_HUB_DIA = 76; // 3" - 76.2
NAB_SPOKE_DIST = 40.65;
// NAB_SPOKE_DIA = 4.5;
NAB_SPOKE_DIA = 5;
REEL_WIDTH = 11;
REEL_TIGHTEN = 1;

$fn=360;

module NH_Base()
{
    difference()
    {
        union()
        {
            for(i=[0:2])
            {
                rotate([0,0,120*i + 30])
                {
                    hull()
                    {
                        cylinder(d=NAB_SPOKE_DIA,h=REEL_WIDTH-REEL_TIGHTEN);
                        translate([NAB_SPOKE_DIST,0,0])
                            cylinder(d=NAB_SPOKE_DIA,h=REEL_WIDTH-REEL_TIGHTEN);
                    }
                    hull()
                    {
                        translate([NAB_SPOKE_DIST-5,0,REEL_WIDTH-REEL_TIGHTEN-0.001])
                        cylinder(d=NAB_SPOKE_DIA*2,h=3);
                        translate([NAB_SPOKE_DIST,0,REEL_WIDTH-REEL_TIGHTEN-0.001])
                            cylinder(d=NAB_SPOKE_DIA*2,h=3);
                    }
                }
            }
            cylinder(d=NAB_HUB_DIA,h=REEL_WIDTH-REEL_TIGHTEN + 3);
        }
        translate([0,0,REEL_WIDTH-REEL_TIGHTEN])
            cylinder(d1=NAB_HUB_DIA-16, d2=NAB_HUB_DIA-10,h=3.001);
        translate([0,0,-0.001])
            cylinder(d=DIN_HUB_DIA,h=REEL_WIDTH-REEL_TIGHTEN + 0.002);
        for(i=[0:2])
        {
            rotate([0,0,120*i + 30])
            {
                hull()
                {
                    translate([0,0,-0.001])
                    cylinder(d=DIN_SPOKE_DIA,h=REEL_WIDTH-REEL_TIGHTEN + 0.002);
                    translate([DIN_SPOKE_DIST,0,-0.001])
                        cylinder(d=DIN_SPOKE_DIA,h=REEL_WIDTH-REEL_TIGHTEN + 0.002);
                }
            }
        }
    }
}

NH_Base();
