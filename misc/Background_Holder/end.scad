$fn=360;

difference()
{
    cylinder(d=30,h=15);
    translate([0,0,-0.001])
    {
        cylinder(d=16.5,h=15.002);
    }
    translate([8,0,7.5])
    {
        cube([7,7,7],center=true);
        rotate([0,90,0])
        {
            cylinder(d=4.5,h=10);
        }
    }
}