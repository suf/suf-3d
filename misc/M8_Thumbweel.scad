$fn=360;


difference()
{
    union()
    {
        intersection()
        {
            cylinder(d=60,h=10);
            translate([0,0,-61+5])
            {
                sphere(d=140);
            }
            translate([0,0,61+5])
            {
                sphere(d=140);
            }
        }
        cylinder(d=22,h=20);
    }
    translate([0,0,-0.001])
    {
        cylinder(d=8.5,h=20.002);
        cylinder(d=15.5,h=7,$fn=6);
    }

    for(i=[0:11])
    {
        rotate([0,0,30*i])
        {
            translate([35,0,0])
            {
                cylinder(d=14,h=10);
            }
        }
    }
}
