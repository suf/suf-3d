$fn=180;
difference()
{
    minkowski()
    {
        cube([34,34,0.001], center = true);
        cylinder(d=4,h=15);
    }
    for(i=[0:3])
        rotate([0,0,45+90*i])
            translate([sqrt(2*pow(15,2)),0,-0.002])
                cylinder(d=3.5,h=15.004);
    translate([0,0,9])
        cylinder(d=17*tan(30)*2, h=8.002, $fn=6);
    translate([0,0,-0.002])
        cylinder(d=11,h=9.004);
}