$fn=180;

module blind_pin()
{
    color("DimGray",1)
    {
        translate([0,-0.5,2.25])
        {
            difference()
            {
                union()
                {
                    translate([-0.75,-1.25,0.249])
                    {
                        cube([1.5,1.25,0.501]);
                    }
                    translate([-0.75,0.249,-2.25])
                    {
                        cube([1.5,0.501,2.25]);
                    }
                    rotate([0,90,0])
                    {
                        cylinder(h=1.5,d=1.5,center=true);
                    }
                }
                translate([-0.76,-2,-2.75])
                {
                    cube([1.52,2,3]);
                }    
                translate([-0.76,-1.25,-3])
                {
                    cube([1.52,1.5,3]);
                }    
                rotate([0,90,0])
                {
                    cylinder(h=1.52,d=0.5,center=true);
                }
            }
        }
    }
}

module pin_bottom()
{
    color("silver",1)
    {
        translate([0,0,-3])
        {
            cylinder(d=0.5,h=3);
        }
    }
}
// =================================================
for(i=[1:2])
{
    translate([(i-1)*-2.54,0,0])
    {
        blind_pin();
    }
}
for(i=[5:45])
{
    translate([(i-1)*-2.54,0,0])
    {
        blind_pin();
    }
}
for(i=[48:49])
{
    translate([(i-1)*-2.54,0,0])
    {
        blind_pin();
    }
}
// =================================================
for(i=[1:2])
{
    translate([(i-1)*-2.54,0,0])
    {
        pin_bottom();
    }
}

for(i=[5:20])
{
    translate([(i-1)*-2.54,0,0])
    {
        pin_bottom();
    }
}

for(i=[31:45])
{
    translate([(i-1)*-2.54,0,0])
    {
        pin_bottom();
    }
}

for(i=[48:49])
{
    translate([(i-1)*-2.54,0,0])
    {
        pin_bottom();
    }
}

// =================================================

translate([-51*2.54,-1.75 -12*2.54,1])
{
    color([0.15,0.15,0.15],1)
    {
        difference()
        {
            cube([54*2.54,12*2.54,5]);
            translate([1.5*2.54,1.5*2.54,2])
            {
                minkowski()
                {
                    cylinder(d=2.54,h=0.001);
                    cube([51*2.54,9*2.54,3.02]);
                }
            }
        }
    }
}

translate([-51*2.54,-1.75 -12*2.54,1])
{
    color("DeepPink",0.2)
    {
        translate([1.5*2.54-0.01,1.5*2.54-0.01,2.01])
        {
            minkowski()
            {
                cylinder(d=2.54,h=0.001);
                cube([51*2.54-0.02,9*2.54-0.02,3.05]);
            }
        }    
    }
}

