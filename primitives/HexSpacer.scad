
module HexSpacer(wrench, hole, height)
{
    difference()
    {
        cylinder(d=wrench*tan(30)*2, h=height, $fn=6);
        translate([0,0,-0.001])
            cylinder(d=hole,h=height+0.002, $fn=180);
    }
}

HexSpacer(5.5,3,10);

